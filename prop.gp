reset

set terminal epslatex color

set xlabel "Skalenteile$\\cdot$1000 []"
set ylabel "Ladung [$\\mu$c]"
set key top left
f(x)=m*x+b
set output 'prop.tex'
set fit logfile "prop.log"
fit f(x) "daten.dat" u (1.238*100)*$7:($8*1000):($9*1000) via m,b
p f(x) t "Regression", "daten.dat" u (1.238*100)*$7:($8*1000):(1000*$9) w e t "Meswerte"
set output
!epstopdf prop.eps
