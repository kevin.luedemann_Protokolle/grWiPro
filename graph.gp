reset

set terminal epslatex color

set xlabel "t [ms]"
set ylabel "U [V]"

f(x)=0.01*exp(-x)
g(x)=150*exp(-x*0.1)*sin(x)

set output "expo.tex"
p f(x) t "Abklingkurve"
set output
!epstopdf expo.eps
set xrange [-10:70]
set output "sinus.tex"
p g(x) t "Abklingkurve"
set output
!epstopdf sinus.eps
