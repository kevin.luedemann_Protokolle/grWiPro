\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {ngerman}
\contentsline {section}{\numberline {1}Einleitung}{1}{section.1}
\contentsline {section}{\numberline {2}Theorie}{1}{section.2}
\contentsline {subsection}{\numberline {2.1}Widerstandsmessung mit dem Ohmschen Gesetz}{1}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Entladung eines Kondensators}{1}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Analoger Stromintegrator}{2}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}RLC-Schwingkreis}{3}{subsection.2.4}
\contentsline {subsection}{\numberline {2.5}Bestimmung der Induktivit\IeC {\"a}t}{4}{subsection.2.5}
\contentsline {section}{\numberline {3}Durchf\"uhrung}{4}{section.3}
\contentsline {section}{\numberline {4}Auswertung}{4}{section.4}
\contentsline {subsection}{\numberline {4.1}Bestimmung von $\epsilon _0$}{4}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Bestimmung von $R_x$ \IeC {\"u}ber die Kondensatorentladung}{5}{subsection.4.2}
\contentsline {section}{\numberline {5}Diskussion}{7}{section.5}
\contentsline {section}{Literatur}{8}{section*.6}
