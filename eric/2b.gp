reset
set term epslatex color solid
set fit logfile "2b.log"
set title "{\\large Entladung vom Kondensator mit parallelgeschaltetem $R_x$ }"
set output "2b.tex"
set xlabel "Zeit $t$ [s]"
set ylabel "Logarithmus der Ladung $\ln(Q(t)/1$ \\textmu C)"
f(x)=m*x+b
fit f(x) "2b.dat" u 1:(log($2/1.00884)):($4)/($2) via m,b
p f(x) t 'Regressionsregrade', "2b.dat" u 1:(log($2/1.00884)):3:($4)/($2) t 'Logarithmus der Ladung' with xyerrorbars
set output 
!epstopdf 2b.eps
