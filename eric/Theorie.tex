\subsection{Widerstandsmessung mit dem Ohmschen Gesetz}
Gewöhnlich misst man Widerstände $R$ mithilfe des Ohmschen Gesetzes $R=U/I$. Für große Widerstände ist also der Strom $I$ sehr gering. Dies is problematisch, da man Ströme erst 
ab einer gewissen Größenordnung messen kann. Für einen genügend großen Strom müsste also die Spannung $U$ sehr groß sein, was aber zu großen elektrischen Feldern führt, 
welche die Messungen verfälschen können.

\subsection{Entladung eines Kondensators}
Betrachtet man einen mit einer Spannung $U_0=RI_0$ aufgeladen Kondensator mit einer Kapazität $C$ so fließt nach \cite[S. 49]{dem2} bei der Entladung der Strom
\begin{align*}
I(t)=-\frac{\d Q}{\d t}=-C\cdot\frac{\d U}{\d t}=\frac{U(t)}{R}
\end{align*}
Die Lösung dieser DGL ist:
\begin{align*}
U(t)=U_0\exp\left(-\frac{1}{RC}\cdot t\right)\aeqiv Q(t)=Q_0\exp\left(-\frac{1}{RC}\cdot t\right)
\end{align*}
Misst man nun die Ladung des Kondensators zu zwei unterschiedlichen Zeitpunkten $t_1$ und $t_2$, so ergibt sich durch Umformung ein Ausdruck für den Widerstand $R$:
\begin{align}
& Q(t_2)=Q(t_1)\exp\left(-\frac{1}{RC}\cdot(t_2-t_1)\right)\nonumber\\
\aeqiv &R=-\frac{t_2-t_1}{C\cdot\ln\left(\frac{Q_2}{Q_1}\right)}=-\frac{1}{C\cdot m}
\label{eq:Rkond}
\end{align}
mit der logarhitmischen Steigung
\begin{align}
m=\frac{\ln Q(t_2)-\ln Q(t_1)}{t_2-t_1}.
\label{eq:mkond}
\end{align}
Hierbei ist zu beachten, dass der Kondensator einen Isolationswiderstand $R_{iso}$ besitzt, weil einige Ladungen über die Luft zwischen den Platten entladen werden können. Die Gesamtkapazität
ist somit der Widerstand des Kondensators $R_C$ parallelgeschaltet mit $R_{iso}$. Es gilt die Beziehung
\begin{align}
\frac{1}{R}=\frac{1}{R_{iso}}+\frac{1}{R_X},
\label{eq:Riso}
\end{align}
wobei $R_X$ der zu messende Widerstand ist.
Der in diesem versuch verwendete Plattenkondensator hat einen Plattenradius von $r=10$\;cm und einen Plattenabstand von $d=0,5$\;cm. Bei diesen Maßen müssen die deutlich erscheienden Randeffekte berücksichtigt werden. Der Kondensator besteht aus $n=65$ Platten.
Nach der Praktikumsanleitung ist der genaue Wert der Kapazität $C$ nach \person{Kirchhoff}:
\begin{align}
C_n=(n-1)\epsilon_0\epsilon_r\left\{\frac{\pi r^2}{d}+r\cdot\left[\ln\left(\frac{16\pi r}{d}\right)-1\right]\right\}
\label{eq:kondensator}
\end{align}
\subsection{Analoger Stromintegrator}
Um die Ladung am Kondensator zu Messen wird ein analoger Stromintegrator verwendet. Er besteht aus einem invertierenden Operationsverstärker, der durch Rückkopplung zum Integrator wird.
\begin{figure}[!h]
 \centering
 \includegraphics[scale=0.7]{Abbildungen/integrator}
 \caption{Aufbau eines analogen Stromintegrators mit einem Operationsverstärker.}
 \label{im:integrator}
\end{figure}
Ideale Operationsverstärker haben einen unendlichen Eingangswiderstand und keinen Ausgangswiderstand. Durch Anwendung der \person{Kirchhoff}schen Regel \cite[S. 55]{dem2} auf den Punkt S erhält man für die Ströme am Kondensator und am Widerstand:
\begin{align*}
I_R+I_C=0
\end{align*}
Es gilt weiterhin nach \cite[S. 19,45]{dem2}
\begin{align*}
I_C=\dot Q_C=C\cdot U_A\\
I_R=\frac{U_E}{R}.
\end{align*}
Durch Integration erhält man für die Ausgangsspannung $U_A$
\begin{align}
U_A&=-\frac{1}{RC}\int\limits_{t_0}^tU_E\;dt\nonumber\\
\aeqiv U_A&\propto \int I_R\;dt=Q
\label{eq:integrator}
\end{align}
Kennt man nach einer Eichung die Proportionalitätskonstante kann man den Integrator also als Ladungsmessgerät benutzen.
\subsection{RLC-Schwingkreis}
\begin{figure}[!h]
 \centering
 \includegraphics{Abbildungen/schwingkreis}
 \caption{Schaltskizze des Schwingkreises}
 \label{im:schwingkreis}
\end{figure}
Im letzen Versuchsteil wird ein RLC-Parallelschwingkreis verwendet, um Widerstände, Kapazitäten und Induktivitäten zu bestimmen.
Durch einen Impulsgenerator wird ein kurzer Spannungsimpuls erzeugt. Nach \cite[S.\;333f]{dem1} und \cite[S.\;170ff]{dem2} gilt für die freie Schwingung eines solchen Schwingkreises:
\begin{align}
\ddot Q+2\beta\dot Q+\omega_0^2Q=0\label{eq:schwingkreisdgl}\\
\beta=\frac{R_L}{2L}\;;\qquad \omega_0=\sqrt{\frac{1}{LC}}\;;\qquad T=\frac{2\pi}{\omega}\;;\qquad\omega=\sqrt{\omega_0^2-\beta^2}.\label{eq:schwinkreisvars}
\end{align}
Hier ist $Q$ die Ladung auf dem Kondensator, $\beta$ die Dämpfungskonstante, $\omega_0$ die Frequenz des ungedämpften kreises, $T$ die Periodendauer und $\omega$ die Frequenz des gedämpften Kreises.
Das logarithmische Dekrement $\Lambda$ ist definiert als der natürliche Logarithmus des inversen Verhältnisses zweier aufeinanderfolgender Maxima:
\begin{align}
\Lambda=\beta\cdot T=\ln\left(\frac{x(t)}{x(t+T)}\right)\label{eq:dekrement}
\end{align}
\subsection{Bestimmung der Induktivität}
Aus den Gleichungung \ref{eq:schwinkreisvars} kann ein Ausdruck für die Induktivität der Spule hergeleitet werden.
\begin{align}
L=\frac{1}{C\omega_0^2}=\frac{1}{C(\omega^2+\beta^2)}=\frac{T^2}{C(4\pi^2+\Lambda^2)}\label{eq:indukt1}
\end{align}
Alternativ gilt für eine Luftspule nach \cite[S. 131]{dem2} die Formel
\begin{align}
L=\mu_0\;A\;\left(\frac{n}{l}\right)^2\label{eq:indukt2}.
\end{align}
